import { connect } from 'react-redux';

import fetchMoviesNumber from '../store/movies-number/actions/fetchMoviesNumber';

const mapStateToProps = (state) => ({
  isLoading: state.totalMovies.isFetching,
  totalMovies: state.totalMovies.data ? state.totalMovies.data.total : null,
});

const mapDispatchToProps = {
  fetchMoviesNumber,
};

export default connect(mapStateToProps, mapDispatchToProps);
