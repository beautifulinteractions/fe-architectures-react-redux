import { connect } from 'react-redux';
import fetchMoviesByTitle from '../store/movies/actions/fetchMoviesByTitle';


const calculateAveragePrice = (avg, item, index, arr) => {
  const price = +(item.ticket.replace('£', ''));
  return (avg + price) / (index === arr.length - 1 ? arr.length : 1);
};


const mapStateToProps = (state) => ({
  movies: state.movies.data,
  isLoading: state.movies.isFetching,
  average: !state.movies.data ? null : state.movies.data.reduce(calculateAveragePrice, 0)
});

const mapDispatchToProps = {
  fetchMoviesByTitle,
};

export default connect(mapStateToProps, mapDispatchToProps);
