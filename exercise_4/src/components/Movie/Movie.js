import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'semantic-ui-react';

/**
 * A single movie object
 */
const Movie = ({ image, title, description, price }) => (
  <Card
    image={image}
    header={title}
    description={description}
    extra={price}
  />
);

Movie.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  price: PropTypes.string,
};

Movie.defaultProps = {
  image: undefined,
  title: 'Movie title',
  description: '',
  price: '£0',
};

export default Movie;
