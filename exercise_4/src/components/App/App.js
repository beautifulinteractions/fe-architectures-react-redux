import React, { useEffect, useState } from 'react';
import { Loader } from 'semantic-ui-react';
import SearchBar from '../SearchBar';
import Movie from '../Movie/Movie';
import MovieList from '../MovieList/MovieList';
import TotalMovies from '../TotalMovies/TotalMovies';
import AveragePrice from '../AveragePrice';
import useMoviesHavingTitle from '../../hooks/useMoviesHavingTitle';
import useMoviesTotal from '../../hooks/useMoviesTotal';

// styles
import './App.css';

/**
 * The Application main entry-point
 */
const App = () => {
  const [movieTitle, setMovieTitle] = useState('');
  const [totalMoviesState, fetchTotalMovies] = useMoviesTotal();
  const [moviesState, fetchMovies] = useMoviesHavingTitle(movieTitle);
  const isLoading = (totalMoviesState.isLoading || moviesState.isLoading);

  // on component did mount, fetch the total movies number
  useEffect(fetchTotalMovies, []);

  const performSearch = () => fetchMovies(true);

  console.log('---- VARIABILI D\'AMBIENTE', process.env);

  return (
    <div className="movie-search">
      {totalMoviesState.total && <TotalMovies total={totalMoviesState.total} />}
      <SearchBar isLoading={isLoading} onChange={e => setMovieTitle(e.target.value)} value={movieTitle}
                 onButtonClick={performSearch} />
      {isLoading && <Loader active inline='centered' />}
      <MovieList>
        {moviesState.movies && moviesState.movies.length > 0 && moviesState.movies.map((movie) => (
          <Movie
            key={movie.id}
            image={movie.image}
            price={movie.ticket}
            description={movie.description}
            title={movie.title}
          />
        ))}
      </MovieList>
      {moviesState.average && <AveragePrice average={moviesState.average} />}
    </div>
  );
};

export default App;
