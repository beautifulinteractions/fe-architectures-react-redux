import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Grid, Icon, Input } from 'semantic-ui-react';

/**
 * The movies search bar
 */
const SearchBar = ({ value, onChange, onButtonClick, isLoading }) => (
  <Card fluid>
    <Card.Content>
      <Card.Header>Movie Search&hellip;</Card.Header>
    </Card.Content>
    <Card.Content>
      <Grid>
        <Grid.Column width={12}>
          <Input placeholder="Movie title" icon="search" fluid onChange={onChange} value={value} disabled={isLoading} />
        </Grid.Column>
        <Grid.Column width={4}>
          <Button fluid color="blue" animated onClick={onButtonClick} disabled={isLoading}>
            <Button.Content visible>Search&hellip;</Button.Content>
            <Button.Content hidden>
              <Icon name='arrow right' />
            </Button.Content>
          </Button>
        </Grid.Column>
      </Grid>
    </Card.Content>
  </Card>
);

SearchBar.propTypes = {
  /**
   * The input value
   */
  value: PropTypes.string,
  /**
   * The input changes callback
   */
  onChange: PropTypes.func,
  /**
   * The button click callback
   */
  onButtonClick: PropTypes.func,
  /**
   * Defines if the input is disabled or not
   */
  isLoading: PropTypes.bool,
};


SearchBar.defaultProps = {
  value: '',
  onChange: undefined,
  onButtonClick: undefined,
  isLoading: false,
};

export default SearchBar;
