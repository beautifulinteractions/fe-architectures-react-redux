import React from 'react';
import PropTypes from 'prop-types';
import { Label, Icon } from 'semantic-ui-react';

/**
 * Shows the total number of movies into the db
 */
const TotalMovies = ({ total }) => (
  <Label>
    <Icon name='tv' />
    We've got {total} movies in our database
  </Label>
);

TotalMovies.propTypes = {
  total: PropTypes.number,
};

TotalMovies.defaultProps = {
  total: 0
};

export default TotalMovies;
