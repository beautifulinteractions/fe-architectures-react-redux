import React from 'react';
import { Card } from 'semantic-ui-react';

const MovieList = ({ children }) => <Card.Group>{children}</Card.Group>;

export default MovieList;
