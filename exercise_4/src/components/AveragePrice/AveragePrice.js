import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'semantic-ui-react';

const AveragePrice = ({ average }) => (
  <div style={{ marginTop: '60px' }}>
    <Label>
      <Icon name='tv' />
      The average ticket price today is: £{average}
    </Label>
  </div>
);

AveragePrice.propTypes = {
  average: PropTypes.number,
};

AveragePrice.defaultProps = {
  average: 0,
};

export default AveragePrice;
