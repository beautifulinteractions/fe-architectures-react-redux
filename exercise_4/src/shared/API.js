const fakeDelay = () => Math.floor(Math.random() * 1200) + 720;

export const mockData = [
  {
    'id': 'mov1',
    'title': 'Kill Bill volume 1',
    'description': 'Suspendisse iaculis, lorem sed pulvinar efficitur, sapien leo sagittis arcu.',
    'image': 'https://placeimg.com/300/400/architecture',
    'ticket': '£7.48'
  },
  {
    'id': 'mov2',
    'title': 'Kill Bill volume 2',
    'description': 'Hendrerit consequat nulla dui at odio. Morbi scelerisque auctor elementum.',
    'image': 'https://placeimg.com/300/440/nature',
    'ticket': '£7.48'
  },
  {
    'id': 'mov3',
    'title': 'Pulp Fiction',
    'description': 'Morbi imperdiet faucibus leo, id placerat sapien. Sed velit felis.',
    'image': 'https://placeimg.com/300/440/architecture',
    'ticket': '£10.00'
  },
  {
    'id': 'mov4',
    'title': 'Once upon a time in America',
    'description': 'Risus molestie, vulputate egestas elit. Praesent leo risus.',
    'image': 'https://placeimg.com/300/440/tech',
    'ticket': '£12.00'
  },
  {
    'id': 'mov5',
    'title': 'Fantozzi',
    'description': 'Placerat quis augue et, sollicitudin laoreet sapien.',
    'image': 'https://placeimg.com/300/440/architecture',
    'ticket': '£7.48'
  },
];

const getMovieByTitle = (title) => new Promise((resolve) => {
  setTimeout(() => resolve({
    query: title,
    status: 200,
    data: mockData
  }), fakeDelay());
});


const getMovieNumber = () => new Promise((resolve) => {
  setTimeout(() => resolve({
    query: 'all movies',
    status: 200,
    data: { total: 20.000 }
  }), fakeDelay());
});

export default {
  getMovieNumber,
  getMovieByTitle,
};

