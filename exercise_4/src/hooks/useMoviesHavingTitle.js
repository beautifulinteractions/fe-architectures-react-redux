import { useDispatch, useSelector } from 'react-redux';
import fetchMoviesByTitle from '../store/movies/actions/fetchMoviesByTitle';

const calculateAveragePrice = (avg, item, index, arr) => {
  const price = +(item.ticket.replace('£', ''));
  return (avg + price) / (index === arr.length - 1 ? arr.length : 1);
};

const useMoviesHavingTitle = (movieTitle) => {
  const dispatch = useDispatch();
  const { data: movies, isFetching: isLoading, error } = useSelector(state => state.movies);
  const average = movies ? movies.reduce(calculateAveragePrice, 0) : null;


  const fetchMovies = () => {
    if (movieTitle && movieTitle.length > 3) {
      dispatch(fetchMoviesByTitle(movieTitle));
    }
  };

  return [{ movies, average, isLoading, error }, fetchMovies]
};

export default useMoviesHavingTitle;
