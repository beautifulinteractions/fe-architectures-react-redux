import { useDispatch, useSelector } from 'react-redux';
import fetchMoviesNumber from '../store/movies-number/actions/fetchMoviesNumber';

const useMoviesHavingTitle = (movieTitle) => {
  const dispatch = useDispatch();
  const { data, isFetching: isLoading, error } = useSelector(state => state.totalMovies);

  const fetchTotalMovies = () => dispatch(fetchMoviesNumber(movieTitle));

  return [{ total: data ? data.total : null, isLoading, error }, fetchTotalMovies]
};

export default useMoviesHavingTitle;
