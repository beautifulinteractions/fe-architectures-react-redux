import { combineReducers } from 'redux';

import moviesNumberReducer from './movies-number/reducers/moviesNumberReducer';
import moviesReducer from './movies/reducers/moviesReducer';

/**
 * Combined reducers
 */
export default combineReducers({
  totalMovies: moviesNumberReducer,
  movies: moviesReducer,
});
