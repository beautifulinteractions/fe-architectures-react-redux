export const MOVIES_NUMBER_GET = 'movies/number/get';
export const MOVIES_NUMBER_RESPONSE = 'movies/number/response';
export const MOVIES_NUMBER_INVALIDATE = 'movies/number/invalidate';

export const MOVIES_SEARCH_GET = 'movies/search/get';
export const MOVIES_SEARCH_RESPONSE = 'movies/search/response';
export const MOVIES_SEARCH_INVALIDATE = 'movies/search/invalidate';
