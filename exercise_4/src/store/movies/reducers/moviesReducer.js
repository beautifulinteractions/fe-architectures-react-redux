import { MOVIES_SEARCH_GET, MOVIES_SEARCH_RESPONSE, MOVIES_SEARCH_INVALIDATE } from '../../actionTypes';

const initialState = {
  isFetching: false,
  didInvalidate: false,
  data: null,
  error: null,
};

const moviesReducer = (state = initialState, action) => {
  switch (action.type) {
    case MOVIES_SEARCH_GET:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
        error: null,
      };
    case MOVIES_SEARCH_RESPONSE:
      return {
        ...state,
        isFetching: false,
        data: action.payload.movies,
      };
    case MOVIES_SEARCH_INVALIDATE:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
        error: action.error,
      };
    default:
      return state;
  }
};

export default moviesReducer;
