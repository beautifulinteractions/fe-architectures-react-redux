import { MOVIES_SEARCH_GET, MOVIES_SEARCH_RESPONSE, MOVIES_SEARCH_INVALIDATE } from '../../actionTypes';
import API from '../../../shared/API';

/**
 * Movies search GET
 */
const moviesSearchGet = () => ({
  type: MOVIES_SEARCH_GET,
});

/**
 * Movies search response
 */
const moviesSearchResponse = (movies) => ({
  type: MOVIES_SEARCH_RESPONSE,
  payload: { movies },
});

/**
 * Movies search did invalidate
 */
const moviesSearchinvalidate = (error) => ({
  type: MOVIES_SEARCH_INVALIDATE,
  error: error,
});

/**
 * Fetches the movies by a given title and store the data
 * within the application's state
 */
const fetchMoviesByTitle = (title) => {
  return (dispatch) => {
    dispatch(moviesSearchGet());

    API.getMovieByTitle(title)
      .then(response => response.data)
      .then(data => dispatch(moviesSearchResponse(data)))
      .catch(error => dispatch(moviesSearchinvalidate(error.message)))
  }
};

export default fetchMoviesByTitle;
