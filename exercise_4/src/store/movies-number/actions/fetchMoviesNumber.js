import { MOVIES_NUMBER_GET, MOVIES_NUMBER_INVALIDATE, MOVIES_NUMBER_RESPONSE } from '../../actionTypes';
import API from '../../../shared/API';


/**
 * Movies number GET
 */
const moviesNumberGet = () => ({
  type: MOVIES_NUMBER_GET,
});

/**
 * Movies number response
 */
const moviesNumberResponse = (total) => ({
  type: MOVIES_NUMBER_RESPONSE,
  payload: { total },
});

/**
 * Movies number did invalidate
 */
const moviesNumberInvalidate = (error) => ({
  type: MOVIES_NUMBER_INVALIDATE,
  error: error,
});

/**
 * Fetches the movies number from the server and store the data
 * within the application's state
 */
const fetchMoviesNumber = () => {
  return (dispatch) => {
    dispatch(moviesNumberGet());

    API.getMovieNumber()
      .then(response => response.data)
      .then(data => dispatch(moviesNumberResponse(data)))
      .catch(error => dispatch(moviesNumberInvalidate(error.message)))
  }
};

export default fetchMoviesNumber;
