import { MOVIES_NUMBER_GET, MOVIES_NUMBER_RESPONSE, MOVIES_NUMBER_INVALIDATE } from '../../actionTypes';

const initialState = {
  isFetching: false,
  didInvalidate: false,
  data: null,
  error: null,
};

const mnReducer = (state = initialState, action) => {
  switch (action.type) {
    case MOVIES_NUMBER_GET:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
        error: null,
      };
    case MOVIES_NUMBER_RESPONSE:
      return {
        ...state,
        isFetching: false,
        data: action.payload.total,
      };
    case MOVIES_NUMBER_INVALIDATE:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
        error: action.error,
      };
    default:
      return state;
  }
};

export default mnReducer;
