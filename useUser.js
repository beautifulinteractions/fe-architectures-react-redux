import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import fetchUser from '../store/user/actions/fetchUser';

/**
 * Select the current user from the store
 */
const userStateSelector = (state) => state.user;

/**
 * Handles the user data fetch and retrieve business logic
 */
const useUser = () => {
  const dispatch = useDispatch();
  const state = useSelector(userStateSelector);

  // when component did mount, dispatch the fetchUser action.
  useEffect(() => {
    dispatch(fetchUser());
  }, []);

  return {
    user: state.data, // the user's data
    isLoading: state.isFetching, // the fetch state
    error: !!state.error ? state.error : false, // the possible error, set to false if no errors are given
  }
};

export default useUser;

