import React from 'react';
import Layout from '../components/Layout';


/**
 * The homepage component
 */
const Homepage = (props) => {
  console.log('PROPS', props);

  return (
    <Layout title="Homepage">
      <h1>Hello, this is a static rendered web application built with <strong>React</strong></h1>
    </Layout>
  )
};


Homepage.getInitialProps = () => {

  return {
    propCustom: 'hello',
  }
};

export default Homepage;
