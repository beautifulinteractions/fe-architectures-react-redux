import React from 'react';
import Layout from '../../components/Layout';

/**
 * The about page
 */
const AboutPage = () => (
  <Layout title="About page">
    <h1>This is the about page...</h1>
  </Layout>
);

export default AboutPage;

