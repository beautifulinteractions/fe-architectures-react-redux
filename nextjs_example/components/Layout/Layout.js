import React from 'react';
import Head from 'next/head';
import NavigationBar from '../NavigationBar';

const Layout = (props) => (
  <>
    <Head>
      <title>{props.title || 'Page title'}</title>
    </Head>
    <style jsx>{`
        .layout {
          background: rgba(0, 0, 0, .02);
          max-width: 768px;
          border-radius: 4px;
          box-shadow: 0 0 10px rgba(0, 0, 0, .1);
          margin: 20px auto;
          padding: 20px;
        }
      `}
    </style>
    <div className="layout">
      <NavigationBar />
      {props.children}
    </div>
  </>
);


export default Layout;
