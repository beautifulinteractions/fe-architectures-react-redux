import React from 'react';
import Link from 'next/link';

const NavigationBar = () => (
  <>
    <style jsx>{`
      .navigation {
        padding-top: 20px;
        padding-bottom: 20px;
      }
      
      .navigation ul {
        margin: 0;
        padding: 0;
      }
      
      .navigation ul li {
        display: inline-block;
        margin-right: 20px;
        background: #12FFeF;
        padding: 20px;
      }
    `}
    </style>
    <nav className="navigation">
      <ul>
        <li><Link href="/">Homepage</Link></li>
        <li><Link href="/about">About</Link></li>
      </ul>
    </nav>
  </>
);

export default NavigationBar;
