import { CHANGE_EMAIL } from '../../actionTypes';

const changeEmail = (email) => ({
  type: CHANGE_EMAIL,
  payload: { email },
});

export default changeEmail;
