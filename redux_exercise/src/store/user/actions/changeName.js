import { CHANGE_NAME } from '../../actionTypes';

const changeName = (nextName) => ({
  type: CHANGE_NAME,
  payload: { name: nextName },
});

export default changeName;
