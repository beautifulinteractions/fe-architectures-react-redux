import { CHANGE_NAME, CHANGE_EMAIL } from '../../actionTypes';

const initialState = {
  email: 'antonio@beautifulinteractions.com',
  name: 'Antonio',
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_EMAIL:
      return { ...state, email: action.payload.email };
    case CHANGE_NAME:
      return { ...state, name: action.payload.name };
    default:
      return state;
  }
};

export default userReducer;
