import uniqueId from 'lodash/fp/uniqueId';
import { ADD_MESSAGE } from '../../actionTypes';


const addMessage = (content) => ({
  type: ADD_MESSAGE,
  payload: {
    message: { content, id: uniqueId('msg-'), },
  }
});

export default addMessage;
