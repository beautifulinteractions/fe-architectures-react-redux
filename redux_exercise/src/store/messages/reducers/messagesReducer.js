import { ADD_MESSAGE } from '../../actionTypes';

const initialState = {
  items: [],
};

const messagesReducer = (state = initialState, action) => {
  if (action.type === ADD_MESSAGE) {
    return {
      items: [...state.items, action.payload.message],
    };
  }

  return state;
};

export default messagesReducer;

