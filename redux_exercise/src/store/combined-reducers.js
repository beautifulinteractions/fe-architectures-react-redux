import { combineReducers } from 'redux';

// reducers
import user from './user/reducers/userReducer';
import messages from './messages/reducers/messagesReducer';

export default combineReducers({
  user,
  messages,
});
