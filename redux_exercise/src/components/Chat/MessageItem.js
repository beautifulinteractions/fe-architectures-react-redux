import React from 'react';
import PropTypes from 'prop-types';
import { Label } from 'semantic-ui-react';

const MessageItem = ({ user, text }) => (
  <div className="chat-message">
    <Label color="blue">
      {user} says:
      <Label.Detail>{text}</Label.Detail>
    </Label>
  </div>
);

MessageItem.propTypes = {
  user: PropTypes.string,
  text: PropTypes.string,
};

MessageItem.defaultProps = {
  user: '',
  text: '',
};

export default MessageItem;
