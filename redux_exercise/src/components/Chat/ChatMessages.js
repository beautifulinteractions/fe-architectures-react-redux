import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Card, Message } from 'semantic-ui-react';
import MessageItem from './MessageItem';

/**
 * Chat messages
 */
const ChatMessages = ({ user, messages }) => (
  <Card.Content>
    <div className="messages-wrapper">
      {messages.length > 0 && messages.map((message) => (
        <MessageItem text={message.content} user={user} key={message.id} />
      ))}
      {messages.length === 0 && (
        <Message
          icon='chat'
          header='No messages yet'
          content='start chatting with yourself by typing a message and pressing "Enter"'
        />
      )}
    </div>
  </Card.Content>
);

ChatMessages.propTypes = {
  user: PropTypes.string,
  messages: PropTypes.arrayOf(
    PropTypes.shape({
      content: PropTypes.string,
      id: PropTypes.string,
    })
  )
};

ChatMessages.defaultProps = {
  user: '',
};

const mapStateToProps = (state) => ({
  user: state.user.name,
  messages: state.messages.items,
});

export default connect(mapStateToProps)(ChatMessages);
