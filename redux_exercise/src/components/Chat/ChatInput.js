import React, { useState } from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import { Card, Grid, Input, Button } from 'semantic-ui-react';
import addMessage from '../../store/messages/actions/addMessage';

/**
 * The chat input
 */
const ChatInput = (props) => {
  const { sendMessage } = props;
  const [message, setMessage] = useState('');

  // handle the key down event
  const handleKeyDown = ({ keyCode }) => keyCode === 13 && onButtonClick();

  // on click handler
  const onButtonClick = () => {
    // send the message
    sendMessage(message);
    // then reset the input field
    setMessage('');
  };

  return (
    <Card.Content extra>
      <Grid>
        <Grid.Column width={14}>
          <Input
            fluid
            placeholder="Message"
            value={message}
            onChange={e => setMessage(e.target.value)}
            onKeyDown={handleKeyDown}
          />
        </Grid.Column>
        <Grid.Column width={2}>
          <Button circular primary icon="paper plane" onClick={onButtonClick} />
        </Grid.Column>
      </Grid>
    </Card.Content>
  )
};


ChatInput.propTypes = {
  sendMessage: PropTypes.func.isRequired,
};

const mapDispatchToProps = ({
  sendMessage: addMessage,
});


export default connect(null, mapDispatchToProps)(ChatInput);
