import React from 'react';
import { Card } from 'semantic-ui-react';
import ChatInput from './ChatInput';
import ChatMessages from './ChatMessages';

import './chat.css';

/**
 * A chat component, unfortunately you can only chat with yourself.
 * Bloody narcissist.
 */
const Chat = () => (
  <Card fluid className="chat">
    <Card.Content header="Chat with yourself" />
    <ChatMessages />
    <ChatInput />
  </Card>
);

export default Chat;
