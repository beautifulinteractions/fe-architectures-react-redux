import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import changeEmail from '../../store/user/actions/changeEmail';
import changeName from '../../store/user/actions/changeName';

/**
 * User View
 */
const UserView = (props) => {
  const { userName, userEmail, changeName, changeEmail } = props;

  // component did mount
  useEffect(() => {
    changeName('Antonio');
    changeEmail('antonio@beautifulinteractions.com');
  }, []);

  return (
    <>
      <h1>User:</h1>
      <p>name: {userName}</p>
      <p>email: {userEmail}</p>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    userName: state.user.name,
    userEmail: state.user.email,
  }
};


const mapDispatchToProps = ({
  changeName,
  changeEmail
});

export default connect(mapStateToProps, mapDispatchToProps)(UserView);
