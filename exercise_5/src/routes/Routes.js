import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// views
import Home from '../views/Home';
import MovieDetail from '../views/MovieDetail';
import PageNotFound from '../views/PageNotFound';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/movies/:movieId" component={MovieDetail} />
      <Route component={PageNotFound} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
