import React from 'react';
import Header from '../../components/Header';
import Routes from '../../routes';

// styles
import './App.css';

/**
 * The application main entry-point
 */
const App = () => (
  <div className="mockflix">
    {/* show the <Header avatar={} username={} /> component once got user */}
    <div className="page-wrapper">
      <Routes />
    </div>
  </div>
);

export default App;
