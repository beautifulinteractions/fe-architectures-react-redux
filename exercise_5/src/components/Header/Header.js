import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react'

import './Header.css';

/**
 * The application header
 */
const Header = ({ avatar, username }) => (
  <div className="app-header">
    <div className="header-wrapper">
      <Image src={avatar} avatar />
      <span>Welcome, <strong>{username}</strong></span>
    </div>
  </div>
);


Header.propTypes = {
  avatar: PropTypes.string,
  username: PropTypes.string,
};

Header.defaultProps = {
  avatar: '',
  username: '',
};

export default Header;
