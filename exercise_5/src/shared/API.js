const fakeDelay = () => Math.floor(Math.random() * 1200) + 720;

/**
 * Returns the current user
 * @returns {Promise<any>}
 */
const getUser = () => new Promise((resolve) => {
  setTimeout(() => resolve({
    status: 200,
    data: {
      id: 'us-10213',
      name: 'John',
      surname: 'Doe',
      displayName: 'John Doe',
      email: 'john.doe@loremipsum.com',
    }
  }), fakeDelay());
});


export const mockFavouriteMovies = [
  {
    'id': 'mov-1',
    'title': 'Kill Bill volume 1',
    'description': 'Suspendisse iaculis, lorem sed pulvinar efficitur, sapien leo sagittis arcu.',
    'longDescription': 'Aliquam condimentum nunc metus, eu pretium magna faucibus nec. Aenean placerat dui odio, id tincidunt elit rhoncus eu. Cras in euismod neque. Etiam ut bibendum odio. Aenean et pretium ex.',
    'image': 'https://placeimg.com/300/400/architecture',
    'cover': 'https://placeimg.com/1025/300/nature',
    'ticket': '£7.48'
  },
  {
    'id': 'mov-2',
    'title': 'Kill Bill volume 2',
    'description': 'Hendrerit consequat nulla dui at odio. Morbi scelerisque auctor elementum.',
    'longDescription': 'Aliquam erat volutpat. Aliquam condimentum nunc metus, eu pretium magna faucibus nec. Curabitur venenatis tellus leo, sed vulputate mi malesuada vel. Sed id pretium augue, vitae pharetra massa. Aliquam in nibh in justo scelerisque vulputate a in sem. In maximus, augue ac facilisis dignissim, eros ante facilisis odio, ac gravida turpis neque in nisi. Proin non tellus suscipit, condimentum lacus malesuada, sollicitudin purus. Cras pulvinar quam nec nisi tristique, eget porta tellus volutpat. Nullam posuere ornare luctus.',
    'image': 'https://placeimg.com/300/440/nature',
    'cover': 'https://placeimg.com/1025/300/nature',
    'ticket': '£7.48'
  },
  {
    'id': 'mov-3',
    'title': 'Pulp Fiction',
    'description': 'Morbi imperdiet faucibus leo, id placerat sapien. Sed velit felis.',
    'longDescription': 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam erat volutpat. Aliquam condimentum nunc metus, eu pretium magna faucibus nec. Aenean placerat dui odio, id tincidunt elit rhoncus eu. Cras in euismod neque. Etiam ut bibendum odio. Aenean et pretium ex.',
    'image': 'https://placeimg.com/300/440/architecture',
    'cover': 'https://placeimg.com/1025/300/nature',
    'ticket': '£10.00'
  },
  {
    'id': 'mov-4',
    'title': 'Once upon a time in America',
    'description': 'Risus molestie, vulputate egestas elit. Praesent leo risus.',
    'longDescription': 'Curabitur venenatis tellus leo, sed vulputate mi malesuada vel. Sed id pretium augue, vitae pharetra massa. In maximus, augue ac facilisis dignissim, eros ante facilisis odio, ac gravida turpis neque in nisi. Proin non tellus suscipit, condimentum lacus malesuada, sollicitudin purus. Cras pulvinar quam nec nisi tristique, eget porta tellus volutpat. Nullam posuere ornare luctus. Aenean placerat dui odio, id tincidunt elit rhoncus eu. Cras in euismod neque. Etiam ut bibendum odio. Aenean et pretium ex.',
    'image': 'https://placeimg.com/300/440/tech',
    'cover': 'https://placeimg.com/1025/300/nature',
    'ticket': '£12.00'
  },
  {
    'id': 'mov-5',
    'title': 'Fantozzi',
    'description': 'Placerat quis augue et, sollicitudin laoreet sapien.',
    'longDescription': 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam erat volutpat. Aliquam condimentum nunc metus, eu pretium magna faucibus nec.',
    'image': 'https://placeimg.com/300/440/architecture',
    'cover': 'https://placeimg.com/1025/300/nature',
    'ticket': '£7.48'
  },
];

/**
 * Returns all the user's favourite movies
 */
const getUserFavourites = (userId) => new Promise((resolve) => {
  setTimeout(() => resolve({
    query: userId,
    status: 200,
    data: mockFavouriteMovies
  }), fakeDelay());
});


export default {
  getUser,
  getUserFavourites,
};

