export const FAVOURITE_MOVIES_GET = 'mockflix/fave/get';
export const FAVOURITE_MOVIES_RESPONSE = 'mockflix/fave/response';
export const FAVOURITE_MOVIES_INVALIDATE = 'mockflix/fave/invalidate';

export const USER_GET = 'mockflix/user/get';
export const USER_RESPONSE = 'mockflix/user/response';
export const USER_INVALIDATE = 'mockflix/user/invalidate';
