import { combineReducers } from 'redux';

/**
 * Combined reducers
 */
export default combineReducers({
  user: () => 'mock',
  movies: () => 'mock',
});
