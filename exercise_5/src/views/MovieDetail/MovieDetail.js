import React from 'react';
import { Redirect } from 'react-router-dom';

const MovieDetail = (props) => {
  let { user, movie } = props;

  return !user ? <Redirect to="/" /> : (
    <div className="page movie-detail-page">
      <p>Display a movie detail by accessing to its <code>cover</code> and <code>longDescription</code> property</p>
    </div>
  );
};

export default MovieDetail;
