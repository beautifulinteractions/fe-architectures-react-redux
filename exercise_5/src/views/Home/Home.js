import React from 'react';

const Home = () => (
  <div className="page home-page">
    <p>1 - Fetch the user from the API, show a loader in the meanwhile</p>
    <p>2 - Fetch the user's favourite movies, still show a loader in the meanwhile</p>
    <p>3 - Display all the user's favourite movies, when a movie is clicked go to the detail page of that movie</p>
  </div>
);

export default Home;

