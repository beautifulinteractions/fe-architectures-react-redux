import React from 'react';
import RegistrationForm from '../RegistrationForm';

import './App.css';

/**
 * Wraps the application's top level components
 */
const App = () => (
  <div className="App">
    <div className="registration-form-wrapper">
      <RegistrationForm />
    </div>
  </div>
);


export default App;
