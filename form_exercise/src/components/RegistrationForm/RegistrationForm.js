import React, { useState, useEffect } from 'react';
import { Form, Input, Card, Button } from 'semantic-ui-react';
import validEmail from '../../shared/validEmail';
import ErrorMsg from '../ErrorMsg';

/**
 * Handle the registration form
 * @returns {*}
 * @constructor
 */
const RegistrationForm = () => {
  // username field related states
  const [username, setUsername] = useState('');
  const [usernameError, setUsernameError] = useState(false);

  // email field related states
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState(false);

  // address field related states, it doesn't have the error state as this field is not required
  const [address, setAddress] = useState('');

  // form validity
  const [valid, setValidity] = useState(false);

  // username field change handler
  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
    setUsernameError(event.target.value.length < 3);
  };

  // email field change handler
  const handleEmailChange = (event) => {
    setEmail(event.target.value);
    setEmailError(!validEmail(event.target.value));
  };

  /**
   * when the value of the required fields or their errors change, then validate the whole form
   */
  useEffect(() => {
    const formValidity = (username && !usernameError) && (email && !emailError);

    setValidity(formValidity);
  }, [username, usernameError, email, emailError]);

  // a bunch of shared props to be spread into the Input components
  const sharedProps = {
    fluid: true,
    style: { marginTop: '10px', marginBottom: '10px' },
  };

  return (
    <Card className="registration-form">
      <Card.Content header="Registration Form" />
      <Card.Content>
        <Form>
          {/* The name input */}
          <Input
            placeholder="Username (required)"
            value={username}
            onChange={handleUsernameChange}
            icon="user"
            {...sharedProps}
          />
          {usernameError && (<ErrorMsg message="Username must have at least 3 characters" />)}
          {/* The email input */}
          <Input
            placeholder="Email (required)"
            value={email}
            onChange={handleEmailChange}
            icon="envelope"
            required
            {...sharedProps}
          />
          {emailError && (<ErrorMsg message="Email address is not valid" />)}
          <Input
            placeholder="Address"
            value={address}
            onChange={(event) => setAddress(event.target.value)}
            icon="map"
            {...sharedProps}
          />
        </Form>
      </Card.Content>
      <Card.Content extra>
        <Button primary disabled={!valid}>Register&hellip;</Button>
      </Card.Content>
    </Card>
  );
};

export default RegistrationForm;
