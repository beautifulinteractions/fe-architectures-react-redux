import React from 'react';

import './error-msg.css';

/**
 * Wraps a given message within a red colored label
 */
const ErrorMsg = ({ message }) => (<span className="error-msg">{message}</span>);

export default ErrorMsg;

