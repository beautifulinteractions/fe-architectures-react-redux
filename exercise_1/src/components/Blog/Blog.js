import React, { useState, useEffect } from 'react';
import get from 'lodash/fp/get';
import Post from '../Post';
import API from '../../shared/API';

import './Blog.css';

const usePosts = () => {
  const [isFetching, setIsFetching] = useState(false); // the isFetching flag providing the API fetch status
  const [posts, setPosts] = useState(null); // the posts array
  const [error, setError] = useState(null); // the possible API error

  // Fetch the posts on component mount
  useEffect(() => {
    setIsFetching(true);

    API.getBlogPosts()
      .then(get('response.posts'))
      .then(setPosts)
      .catch(setError)
      .finally(() => setIsFetching(false));
  }, []);

  return { posts, isFetching, error };
};


/**
 * Retrieve blog posts on mount and eventually show em in style
 * @constructor
 */
const Blog = () => {
  const data = usePosts();
  const { posts, isFetching, error } = data;

  console.log('data', data);

  return (
    <section className="blog blog-container">
      <h2>Blog posts:</h2>
      { /* Handle posts */}
      {posts && (
        <div className="posts-wrapper">
          {posts.map(({ content, title, thumbnail, id }) => (
            <Post content={content} title={title} image={thumbnail} key={id} />
          ))}
        </div>
      )}
      { /* Handle isFetching */}
      {isFetching && (<p>Fetching data&hellip;</p>)}
      { /* Handle error */}
      {error && (<p style={{ color: 'red' }}>{error.message}</p>)}
    </section>
  );
};

export default Blog;
