import React from 'react';
import PropTypes from 'prop-types';

// styles
import './Post.css';

/**
 * Shows a single blog post
 */
const Post = (props) => {
  const { title, content, image } = props;

  return (
    <article className="blog-post">
      <h4>{title}</h4>
      {image && (
        <div className="blog-thumbnail">
          <img src={image} alt={title} />
        </div>
      )}
      <div className="blog-content">
        {content}
      </div>
    </article>
  );
};

Post.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  image: PropTypes.string
};

Post.defaultProps = {
  image: undefined,
};

export default Post;
