import React from 'react';
import Blog from '../Blog';
import './App.css';

/**
 * Wraps all the page's top level components
 */
const App = () => (
  <div className="exercise-1">
    { /* <header>
      <h1>Exercise 1</h1>
      <p>Edit the <code>Blog</code> component to perform the <code>API.getBlogPosts</code> function on mount.</p>
      <p>Eventually show the response using the <code>Post</code> component.</p>
    </header> */ }
    <Blog />
  </div>
);


export default App;
