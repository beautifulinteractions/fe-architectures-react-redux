/**
 * Simulates a server API fetch returning an array of blog posts
 */
const getBlogPosts = () => new Promise((resolve) => {
  const responseTime = Math.floor(Math.random() * 1000) + 2500;

  setTimeout(() => {
    resolve({
      response: {
        posts: [
          {
            id: 'bp-1',
            title: 'Post title',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer scelerisque urna quis diam egestas, vitae ornare ligula blandit. Duis feugiat ipsum felis, sed facilisis ipsum finibus at. Fusce sagittis felis eu nibh blandit rhoncus. In commodo ex a risus aliquam consectetur. Quisque nec elementum tellus. Vestibulum pharetra vehicula dolor quis finibus. Proin at imperdiet leo. Donec eu ex eu nibh congue blandit.',
            thumbnail: 'https://picsum.photos/120/120',
          },
          {
            id: 'bp-2',
            title: 'Post title 2',
            content: 'Nunc nec vestibulum nisi, ullamcorper accumsan neque. Duis vel bibendum augue, et ornare augue. Pellentesque tempor pellentesque mauris a mattis. Donec rhoncus mauris ipsum, eget facilisis nunc efficitur sit amet.',
            thumbnail: 'https://picsum.photos/120/120',
          },
          {
            id: 'bp-3',
            title: 'Post title 3',
            content: 'Aliquam placerat faucibus purus, nec pulvinar purus. Aliquam erat volutpat. Donec gravida velit justo, vel fringilla augue ultricies vitae. Morbi eget dapibus lorem. Suspendisse viverra lacinia nisi, et dapibus turpis dignissim quis. Morbi eu elementum velit.',
            thumbnail: 'https://picsum.photos/120/120',
          },
          {
            id: 'bp-4',
            title: 'Post title 4',
            content: 'Nam rhoncus vehicula auctor. Nunc quis nulla ac sem imperdiet porta sed in velit. Duis sed erat et lectus aliquam posuere vitae non ligula. Cras interdum erat ac nisl aliquam vestibulum. Morbi quis varius metus, in viverra augue. Donec vitae mattis ante.',
            thumbnail: 'https://picsum.photos/120/120',
          },
        ]
      }
    });
  }, responseTime);
});


export default Object.freeze({
  getBlogPosts,
});
