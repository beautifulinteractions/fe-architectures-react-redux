import React, { PureComponent } from 'react';

class PureComp extends PureComponent {
  render() {
    console.log('--- RENDERING PURE COMPONENT --- ');
    return (
      <h1>Hello, I'm a pure component</h1>
    );
  }
}

export default PureComp;
