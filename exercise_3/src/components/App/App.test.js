import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import PureComp from '../PureComp/PureComp';
import StandardComp from '../StandardComp/StandardComp';

describe('App component', () => {
  it('should render without explode', () => {
    shallow(<App />)
  });

  it('should possibly have a defined initial state', () => {
    const wrapper = shallow(<App />);
    const state = wrapper.state();

    expect(state).toEqual({
      name: 'Antonio',
      counter: 0,
    })
  });

  it('should possibly have default props', () => {
    expect(App.defaultProps).toEqual({ name: 'Antonio' })
  });

  it('should render PureComp', () => {
    const wrapper = shallow(<App />);

    expect(wrapper.find(PureComp).length).toBe(1);
  });

  it('should render PureComp', () => {
    const wrapper = shallow(<App />);

    expect(wrapper.find(StandardComp).length).toBe(1);
  });

  it('should render a div with .exercise-page class', () => {
    const wrapper = shallow(<App />);

    expect(wrapper.find('.exercise-page').length).toBe(1);
  });
});
