import React, { Component } from 'react';
import PureComp from '../PureComp/PureComp';
import StandardComp from '../StandardComp/StandardComp';

import './App.css';

/**
 * Wraps all the page's top level components
 */
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: props.name,
      counter: 0,
    }
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({
        name: 'Antonio',
        counter: this.state.counter + 1
      });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { counter, name } = this.state;
    console.log('--- Rendering: App Component ---');

    return (
      <div className="exercise-page">
        <header>
          <h1>Exercise 3</h1>
          <p>This page is rendering every second, in order to see the differences between a
            standard <code>Component</code><br />
            and a <code>PureComponent</code>, create two different components extending from those classes.
          </p>
          <p>Put a <code>condole.log</code> in the render method and check it out.</p>
          <hr />
          <p>Rendered <strong>{counter}</strong> times</p>
        </header>
        <PureComp name={name} />
        <StandardComp name={name} />
      </div>
    )
  }
}


App.defaultProps = {
  name: 'Antonio'
};

export default App;
