import React, { Component } from 'react';

class StandardComp extends Component {
  render() {
    console.log('--- RENDERING A STANDARD COMPONENT --- ');
    return (
      <h1>Hello, I'm a standard component</h1>
    );
  }
}

export default StandardComp;
