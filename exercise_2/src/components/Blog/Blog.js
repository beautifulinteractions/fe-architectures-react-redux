import React, { Component } from 'react';
import get from 'lodash/fp/get';
import Post from '../Post';
import API from '../../shared/API';

import './Blog.css';

/**
 * Retrieve blog posts on mount and eventually show em in style
 * @constructor
 */
class Blog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: null,
      loading: true,
      error: false,
    };
  }

  componentDidMount() {
    API.getBlogPosts()
      .then(get('response.posts'))
      .then((posts) => this.setState({ posts }))
      .finally(() => this.setState({ loading: false }));
  }

  handleLike = (postId) => {
    API.postLike(postId)
      .then(get('response.posts'))
      .then((posts) => this.setState({ posts }));
  };

  render() {
    const { posts, loading } = this.state;

    return (
      <section className="blog blog-container">
        <h2>Blog posts:</h2>
        {loading && <p>is fetching data&hellip;</p>}
        {posts && posts.map(({ title, content, id, thumbnail, liked }) => (
          <Post title={title} content={content} key={id} image={thumbnail} liked={liked} onLikeClick={() => this.handleLike(id)} />
        ))}
      </section>
    );
  }
}

export default Blog;
