import React, { Component } from 'react';
import Blog from '../Blog';
import './App.css';

/**
 * Wraps all the page's top level components
 */
class App extends Component {
  render() {
    return (
      <div className="exercise-page">
        <header>
          <h1>Exercise 2</h1>
          <p>Edit the <code>Blog</code> class component to perform the <code>API.getBlogPosts</code> function on mount.
          </p>
          <p>Eventually show the response using the <code>Post</code> component.</p>
          <hr/>
          <p>Implements the business logic for a like button using <code>API.postLike(postId: string)</code>.</p>
        </header>
        <Blog />
      </div>
    )
  }
}

export default App;
