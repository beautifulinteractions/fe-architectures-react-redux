import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Button.css';

/**
 * A poorly styled button to be reused within this app
 */
class Button extends Component {
  static propTypes = {
    onClick: PropTypes.func,
  };

  static defaultProps = {
    onClick: undefined,
  };

  render() {
    const { children, onClick } = this.props;

    return (
      <button className="bi-btn" onClick={onClick}>
        {children}
      </button>
    );
  }
}

export default Button;
