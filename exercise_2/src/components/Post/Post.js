import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

// styles
import './Post.css';


/**
 * Shows a single blog post
 */
class Post extends Component {

  static propTypes = {
    /**
     * The post title
     */
    title: PropTypes.string.isRequired,
    /**
     * The post content
     */
    content: PropTypes.string.isRequired,
    /**
     * The possible post image
     */
    image: PropTypes.string,
    /**
     * The possible like button onClick callback
     */
    onLikeClick: PropTypes.func,
    /**
     * Defines if the post has been liked or not, if so shows a label
     */
    liked: PropTypes.bool,
  };

  static defaultProps = {
    image: undefined,
    onLikeClick: undefined,
    liked: false,
  };

  handleItemClick = () => {
    const { onLikeClick, title, content, image } = this.props;

    if (onLikeClick) {
      onLikeClick({ title, content, image });
    }
  };

  render() {
    const { title, content, liked, image } = this.props;

    return (
      <article className="blog-post">
        <h4>{title}</h4>
        {image && (
          <div className="blog-thumbnail">
            <img src={image} alt={title} />
          </div>
        )}
        <div className="blog-content">
          {content}
        </div>
        <strong>
          {liked && <p>You like this post!</p>}
          {!liked && <p>You don't this post yet...</p>}
        </strong>
        <Button onClick={this.handleItemClick}>
          {liked ? 'Stop liking' : 'Like it'}
        </Button>
      </article>
    );
  }
}


export default Post;
